package main

import (
	"fmt"
	"math/rand"
	"time"
)

// initiate the text wanted when the state is 1
var code string = "■"

func (board *Board) displayBoard() {

	for i := 0; i < ROWS; i++ {
		for j := 0; j < COLS; j++ {
			if j%COLS == 0 {
				fmt.Println()
			}
			if board[i][j] == false {
				fmt.Printf(" ")
			} else {
				fmt.Print(code)
			}
		}
	}

}

func (board *Board) update() Board {

	// we need a buffer board
	var newBoard = *new(Board)

	for i := 0; i < ROWS; i++ {
		for j := 0; j < COLS; j++ {

			// cast bool state as an int
			var state = b2i(board[i][j])
			var neighbors int = board.getNeighborsAmount(i, j)

			// apply Conway's rules and cast to the new board
			if state == 0 && neighbors == 3 {
				newBoard[i][j] = true

			} else if state == 1 && neighbors > 3 {
				newBoard[i][j] = false

			} else if state == 1 && neighbors < 2 {
				newBoard[i][j] = false

			} else if state == 1 && (neighbors == 2 || neighbors == 3) {
				newBoard[i][j] = true

			} else {
				newBoard[i][j] = board[i][j]
			}

		}

	}
	return newBoard
}

func (board *Board) getNeighborsAmount(x, y int) int {

	var amount int = 0
	for l := -1; l < 2; l++ {
		for k := -1; k < 2; k++ {
			row := (l + x + ROWS) % ROWS
			col := (k + y + COLS) % COLS
			amount += b2i(board[row][col])

		}

	}

	if board[x][y] == true {

		amount -= b2i(board[x][y])
	}
	return amount
}

// loop through the board aand assign 'randomly' a boolean value
func (board *Board) randomize() Board {
	for i := 0; i < ROWS; i++ {
		for j := 0; j < COLS; j++ {
			board[i][j] = RandBool()

		}

	}

	return *board
}

// Short little helper functions

// return a random true of false value
func RandBool() bool {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2) == 1
}

// transforms a bool into a 0-1 int
func b2i(b bool) int {
	if b {
		return 1
	}
	return 0
}
