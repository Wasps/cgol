package main

import (
	"time"

	"github.com/inancgumus/screen"
)

func clear() {

	// 75 Millisecs seems to be a good compromise
	time.Sleep(time.Millisecond * 75)
	screen.MoveTopLeft()
	screen.Clear()
}
