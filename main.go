package main

import (
	"flag"
)

const (
	ROWS = 30
	COLS = 100
)

type Board [ROWS][COLS]bool

var board = *new(Board)

func main() {

	// parse the life argument
	nFlag := flag.Int("life", 100, "Amount of iterations")
	flag.Parse()
	life := *nFlag

	board.randomize()
	for i := 0; i < life; i++ {
		board = board.update()
		board.displayBoard()
		clear()

	}

}
