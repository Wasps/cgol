module cgol

go 1.18

require github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3

require (
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
